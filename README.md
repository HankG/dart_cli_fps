# Dart Console/Command Line FPS Demo

This is a Dart version of the C++ Windows Console FPS demo 
OneLoneCoder(Javidx9) put together. The original project was 
a "just because we can" type experiment because he "had a 
couple of hours to kill". I wanted to see how well that could
be translated to a Dart CLI program. And here we are...

He made [this YouTube video](https://www.youtube.com/watch?v=xW8skO7MFYw) of the
development. 

He also posted the code [in this GitHub repo](https://github.com/OneLoneCoder/CommandLineFPS).

The originla code is licensed GNU GPLv3.0 and therefore so is this.

## How to Use

Make sure you have a Dart development environment installed. 
Follow [the instructions on the main site](https://dart.dev/get-dart)
if you don't.

Then check out the source code:

```bash
git clone https://gitlab.com/HankG/dart_cli_fps.git
```

Set your terminal window to **exactly** 120x40. Then go into the directory and run:

```bash
cd dart_cli_fps
dart run
```

The navigation keys:
* W moves forward
* S moves backward
* A rotates counter-clockwise
* D rotates clockwise
* ESC key or CTRL-C quits the program

## Screenshots

Linux Mint with standard terminal:

![Linux Mint Screenshot](screenshots/dart_cli_fps_linux.png)

## Movies

Linux Mint with standard terminal:

![Linux Mint Movie](screenshots/dart_cli_fps_linux.mp4)

Windows 10 in Command Line (doesn't seem to work in git bash shell
because it doesn't pick up the changed resolution of the terminal window):

![Windows Movie](screenshots/dart_cli_fps_windows.mp4)

MacOS with the standard terminal:

![Mac movie](screenshots/dart_cli_fps_macos.mp4)