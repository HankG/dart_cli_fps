/*
	GNU GPLv3
	https://github.com/OneLoneCoder/videos/blob/master/LICENSE
 */
import 'dart:io';
import 'dart:math';

import 'package:collection/collection.dart';
import 'package:dart_console2/dart_console2.dart';

// Convert to Dart from C++ version at: https://github.com/OneLoneCoder/CommandLineFPS/blob/master/CommandLineFPS.cpp

//Key press information
enum KeyTypes {
  a([97]),
  A([65]),
  d([100]),
  D([68]),
  w([119]),
  W([87]),
  s([115]),
  S([83]),
  Esc([27]),
  CtrlC([3]),
  unknown([]),
  ;

  final List<int> codes;
  const KeyTypes(this.codes);

  static KeyTypes fromValue(List<int> value) {
    final eq = ListEquality();
    return values.firstWhere(
      (e) => eq.equals(e.codes, value),
      orElse: () => unknown,
    );
  }
}

// Int value of Unicode map characters
final spaceCode = ' '.codeUnits.first;
final poundCode = '#'.codeUnits.first;
final xCode = 'x'.codeUnits.first;
final periodCode = '.'.codeUnits.first;
final dashCode = '-'.codeUnits.first;
final playerCode = 'P'.codeUnits.first;

const nScreenWidth = 120; // Console Screen Size X (columns)
const nScreenHeight = 40; // Console Screen Size Y (rows)
const nMapWidth = 16; // World Dimensions
const nMapHeight = 16;
const margin = 1.2;
const minX = margin;
const minY = margin;
const maxX = nMapWidth - margin;
const maxY = nMapHeight - margin;
const fFOV = 3.14159 / 4.0; // Field of View
const fDepth = 16.0; // Maximum rendering distance
const map = ''
    '################'
    '#..............#'
    '#.......########'
    '#..............#'
    '#......##......#'
    '#......##......#'
    '#..............#'
    '###............#'
    '##.............#'
    '#......####..###'
    '#......#.......#'
    '#......#.......#'
    '#..............#'
    '#......#########'
    '#..............#'
    '################';
final mapAsCodes = map.codeUnits;

void main() async {
  final delayMicroseconds =
      Platform.isLinux ? 10 : 1000; // Loop delay to listen for keyboard
  final fSpeed = Platform.isLinux ? 5.0 : 0.05; // Walking Speed

  final screen = List.generate(
    nScreenWidth * nScreenHeight,
    (index) => spaceCode,
  );

  var fPlayerX = 14.7; // Player Start Position
  var fPlayerY = 5.09;
  var fPlayerA = 0.0; // Player Start Rotation
  var fElapsedTime = 0.0;

  final console = Console();
  if (console.windowWidth != nScreenWidth ||
      console.windowHeight != nScreenHeight) {
    console.writeLine('Terminal window must be exactly:'
        '$nScreenWidth x $nScreenHeight');
    console.writeLine('This terminals dimensions are:'
        '${console.windowWidth} x ${console.windowHeight}');
    exit(-1);
  }
  console.clearScreen();
  console.resetCursorPosition();
  console.rawMode = true;

  var running = true;
  stdin.listen(
    (keyValues) {
      try {
        final key = KeyTypes.fromValue(keyValues);
        switch (key) {
          case KeyTypes.a:
          case KeyTypes.A:
            // CCW Rotation
            fPlayerA -= (fSpeed * 0.75) * fElapsedTime;
            break;
          case KeyTypes.d:
          case KeyTypes.D:
            // CW Rotation
            fPlayerA += (fSpeed * 0.75) * fElapsedTime;
            break;
          case KeyTypes.w:
          case KeyTypes.W:
            //Forward movement and collisions
            fPlayerX += sin(fPlayerA) * fSpeed * fElapsedTime;
            fPlayerY += cos(fPlayerA) * fSpeed * fElapsedTime;
            final index = fPlayerX.toInt() * nMapWidth + fPlayerY.toInt();
            if (map[index] == '#') {
              fPlayerX -= sin(fPlayerA) * fSpeed * fElapsedTime;
              fPlayerY -= cos(fPlayerA) * fSpeed * fElapsedTime;
            }
            break;
          case KeyTypes.s:
          case KeyTypes.S:
            // Backward movement and collision
            fPlayerX -= sin(fPlayerA) * fSpeed * fElapsedTime;
            fPlayerY -= cos(fPlayerA) * fSpeed * fElapsedTime;
            final index = fPlayerX.toInt() * nMapWidth + fPlayerY.toInt();
            if (map[index] == '#') {
              fPlayerX += sin(fPlayerA) * fSpeed * fElapsedTime;
              fPlayerY += cos(fPlayerA) * fSpeed * fElapsedTime;
            }

            break;
          case KeyTypes.Esc:
          case KeyTypes.CtrlC:
            // Close the program
            running = false;
            break;
          case KeyTypes.unknown:
          default:
            break;
        }
      } catch (_) {}
    },
    onError: (error) => console.writeLine(error),
    onDone: () => console.writeLine('DONE'),
  );

  var tp1 = DateTime.timestamp();
  var tp2 = DateTime.timestamp();

  try {
    while (running) {
      tp2 = DateTime.timestamp();
      fElapsedTime = tp2.difference(tp1).inMicroseconds / 1000.0;
      tp1 = tp2;

      if (fPlayerX < minX) {
        fPlayerX = minX;
      }

      if (fPlayerY < minY) {
        fPlayerY = minY;
      }

      if (fPlayerX > maxX) {
        fPlayerX = maxX;
      }

      if (fPlayerY > maxY) {
        fPlayerY = maxY;
      }
      for (int x = 0; x < nScreenWidth; x++) {
        // For each column, calculate the projected ray angle into world space
        final fRayAngle = (fPlayerA - fFOV / 2.0) +
            (x.toDouble() / nScreenWidth.toDouble()) * fFOV;

        // Find distance to wall
        final fStepSize =
            0.1; // Increment size for ray casting, decrease to increase
        var fDistanceToWall =
            0.0; //                                      resolution

        bool bHitWall = false; // Set when ray hits wall block
        bool bBoundary =
            false; // Set when ray hits boundary between two wall blocks

        final fEyeX = sin(fRayAngle); // Unit vector for ray in player space
        final fEyeY = cos(fRayAngle);

        // Incrementally cast ray from player, along ray angle, testing for
        // intersection with a block
        while (!bHitWall && fDistanceToWall < fDepth) {
          fDistanceToWall += fStepSize;
          final nTestX = (fPlayerX + fEyeX * fDistanceToWall).toInt();
          final nTestY = (fPlayerY + fEyeY * fDistanceToWall).toInt();

          // Test if ray is out of bounds
          if (nTestX < 0 ||
              nTestX >= nMapWidth ||
              nTestY < 0 ||
              nTestY >= nMapHeight) {
            bHitWall = true; // Just set distance to maximum depth
            fDistanceToWall = fDepth;
          } else {
            // Ray is inbounds so test to see if the ray cell is a wall block
            if (map[nTestX * nMapWidth + nTestY] == '#') {
              // Ray has hit wall
              bHitWall = true;

              // To highlight tile boundaries, cast a ray from each corner
              // of the tile, to the player. The more coincident this ray
              // is to the rendering ray, the closer we are to a tile
              // boundary, which we'll shade to add detail to the walls
              final p = <(double, double)>[];

              // Test each corner of hit tile, storing the distance from
              // the player, and the calculated dot product of the two rays
              for (int tx = 0; tx < 2; tx++) {
                for (int ty = 0; ty < 2; ty++) {
                  // Angle of corner to eye
                  double vy = nTestY.toDouble() + ty - fPlayerY;
                  double vx = nTestX.toDouble() + tx - fPlayerX;
                  double d = sqrt(vx * vx + vy * vy);
                  double dot = (fEyeX * vx / d) + (fEyeY * vy / d);
                  p.add((d, dot));
                }
              }

              // Sort Pairs from closest to farthest
              p.sort((p1, p2) => p1.$1.compareTo(p2.$1));

              // First two/three are closest (we will never see all four)
              double fBound = 0.01;
              if (acos(p[0].$2) < fBound) bBoundary = true;
              if (acos(p[1].$2) < fBound) bBoundary = true;
              if (acos(p[2].$2) < fBound) bBoundary = true;
            }
          }
        }

        // Calculate distance to ceiling and floor
        int nCeiling =
            ((nScreenHeight / 2.0) - nScreenHeight / (fDistanceToWall)).toInt();
        int nFloor = nScreenHeight - nCeiling;

        // Shader walls based on distance
        int nShade = spaceCode;
        if (fDistanceToWall <= fDepth / 4.0) {
          nShade = 0x2588; // Very close
        } else if (fDistanceToWall < fDepth / 3.0) {
          nShade = 0x2593;
        } else if (fDistanceToWall < fDepth / 2.0) {
          nShade = 0x2592;
        } else if (fDistanceToWall < fDepth) {
          nShade = 0x2591;
        } else {
          nShade = spaceCode; // Too far away
        }

        if (bBoundary) nShade = spaceCode; // Black it out

        for (int y = 0; y < nScreenHeight; y++) {
          // Each Row
          if (y <= nCeiling) {
            screen[y * nScreenWidth + x] = spaceCode;
          } else if (y > nCeiling && y <= nFloor) {
            screen[y * nScreenWidth + x] = nShade;
          } else // Floor
          {
            // Shade floor based on distance
            final b = 1.0 -
                ((y.toDouble() - nScreenHeight / 2.0) /
                    (nScreenHeight.toDouble() / 2.0));
            if (b < 0.25) {
              nShade = poundCode;
            } else if (b < 0.5) {
              nShade = xCode;
            } else if (b < 0.75) {
              nShade = periodCode;
            } else if (b < 0.9) {
              nShade = dashCode;
            } else {
              nShade = spaceCode;
            }
            screen[y * nScreenWidth + x] = nShade;
          }
        }
      }

      // Display Map
      for (int nx = 0; nx < nMapWidth; nx++) {
        for (int ny = 0; ny < nMapWidth; ny++) {
          screen[(ny + 1) * nScreenWidth + nx] =
              mapAsCodes[ny * nMapWidth + nx];
        }
        screen[(fPlayerX.toInt() + 1) * nScreenWidth + fPlayerY.toInt()] =
            playerCode;
      }

      final stats =
          'X=${fPlayerX.toStringAsFixed(2)}, Y=${fPlayerY.toStringAsFixed(2)}, A=${fPlayerA.toStringAsFixed(2)}, FPS=${(1000.0 / fElapsedTime).toStringAsFixed(2)}';
      console.resetCursorPosition();
      console.write(String.fromCharCodes(screen));
      console.resetCursorPosition();
      console.writeLine(stats);
      await Future.delayed(Duration(microseconds: delayMicroseconds));
    }
  } catch (e) {
    console.writeLine(
        'X=${fPlayerX.toStringAsFixed(2)}, Y=${fPlayerY.toStringAsFixed(2)}, A=${fPlayerA.toStringAsFixed(2)}');
    console.rawMode = false;
    rethrow;
  }
  console.rawMode = false;
  console.clearScreen();
  exit(0);
}
